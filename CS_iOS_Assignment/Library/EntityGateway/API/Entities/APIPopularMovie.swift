//
//  APIPopularMovie.swift
//  CS_iOS_Assignment
//
//  Created by Gokul Narasimhan on 2021-01-25.
//  Copyright © 2021 Backbase. All rights reserved.
//

import Foundation

struct APIPopularMovie: Decodable {
    var results: [APIPopularMovieResult]
    var page: Int
    
    enum CodingKeys: String, CodingKey {
        case results = "results"
        case page = "page"
    }
}

struct APIPopularMovieResult: Decodable {
    var title: String?
    var rating: Double?
    var releaseDate: String?
    var poster: String?
    
    enum CodingKeys: String, CodingKey {
          case title = "title"
          case rating = "vote_average"
          case poster = "poster_path"
          case releaseDate = "release_date"
    }
}
